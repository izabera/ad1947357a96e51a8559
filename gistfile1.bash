urlencode() {
    # urlencode <string>
    local LC_ALL=C c i n IFS=
    if (( (n = ${#1}) > 30 )); then
        printf %s "$1" |
        while read -rN1 c; do
            if [[ $c = [[:alnum:].~_-] ]]; then printf %s "$c"
            else printf %%%02X "'$c"
            fi
        done
    else
        for (( i = 0; i < n; i++ )); do
            c=${1:i:1}
            if [[ $c = [[:alnum:].~_-] ]]; then printf %s "$c"
            else printf %%%02X "'$c"
            fi
        done
    fi
}

urldecode() {
    # urldecode <string>
    printf '%b' "${1//%/\\x}"
}
